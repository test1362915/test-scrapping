## Technical test

### Description
This project is a Python / Django application allowing to launch a scrapper with a name passed in parameter.
The application has a web page with a simple form, the collected data are stored in DB and accessible via the administration interface of Django.

### Requirement

You need to install RabitMQ on your OS:
- [RabbitMQ installation](https://www.rabbitmq.com/download.html)


### Installation

###### _"Classic way"_
- Clone the project
- Create a virtualenv
- Install requirements :
```sh
(env)$ pip install -r requirements.txt 
```
- Configure the database:
```sh
(env)$ cd test-scrapping/django-celery-rabbitmq
(env)$ python manage.py makemigrations
(env)$ python manage.py migrate
(env)$ python manage.py createsuperuser  # create admin profil 
```
- Start the app:
```sh
(env)$ python manage.py runserver
```
- In an new terminal, start RabbitMQ server:
```sh
(env)$ celery -A mysite worker --loglevel=INFO
```
###### _"With Docker"_

- Create Docker image and start container:
```sh
(env)$ docker build -t mysite .
(env)$ docker-compose up
```
