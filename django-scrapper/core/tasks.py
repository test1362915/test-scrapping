from celery import shared_task
from .models import TelegramUser, Channel

api_id = "SECRET"
api_hash = 'SECRET'
phone = "SECRET"
username = "SECRET"


@shared_task
def collect_data(all_users, channel_name):
    print('Start scrapping data')
    if not Channel.objects.filter(name=channel_name).exists():
        Channel(
            name=channel_name
        ).save()

    channel = Channel.objects.get(name=channel_name)

    for user_info in all_users:
        user = TelegramUser(
            user_id=user_info.get('user.id'),
            first_name=user_info.get('first_name'),
            last_name=user_info.get('last_name'),
            username=user_info.get('username'),
            phone=user_info.get('phone'),
            access_hash=user_info.get('user.access_hash'),
            is_bot=user_info.get('is_bot'),
            channel=channel,
        )
        user.save()
    print("Scrapping done")
