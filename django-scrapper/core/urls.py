from django.urls import path, include
from .views import ajax_view
from . import views
from django.contrib import admin

urlpatterns = [
    path("", views.index, name="index"),
    path('ajax/', ajax_view, name='ajax-view'),
]

admin.site.site_header = 'TelegramScrapper'
admin.site.index_title = 'TelegramScrapper'
admin.site.site_title = 'TelegramScrapper'
