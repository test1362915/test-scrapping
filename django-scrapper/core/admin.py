from django.contrib import admin
from .models import TelegramUser, Channel


@admin.register(TelegramUser)
class TelegramUser(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'access_hash', 'is_bot', 'phone', 'created_at', 'channel')
    list_filter = ('is_bot', 'channel')
    search_fields = ('user_id', 'first_name', 'last_name', 'phone')


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created_at')
    search_fields = ('name',)
