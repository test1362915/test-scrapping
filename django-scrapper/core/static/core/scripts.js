alert("test")

$(document).ready(function() {
    $("#my-form").submit(function(event) {
        event.preventDefault();

        var inputText = $("#scrap-input").val();

        $.ajax({
            type: "POST",
            url: "/ajax/",
            data: inputText,
            beforeSend: function(xhr, settings) {
            $('#scrap-input').val('');
                var csrftoken = getCookie('csrftoken');
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
                $('#loader').show();

            },
            success: function(data) {
                console.log("Success:", data);
                $('#loader').hide();
                $('#message').html('Scrapping lancé, vous serez notifié par mail une fois terminé').css('color', 'green');
                $('#message').show()

            },
            error: function(xhr, status, error) {
                console.log("Error:", error);
                $('#message').html('Erreur, un problème est survenu.').css('color', 'red');

            }
        });
    });
});

// function to get the CSRF token from the cookie
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
