from django.shortcuts import render
from django.http import JsonResponse
from .tasks import collect_data
from telethon import functions, types
from .models import Channel

from telethon import TelegramClient
from telethon.errors import SessionPasswordNeededError


#### (Reminder) Celery command
# celery -A mysite worker --loglevel=INFO

def index(request):
    return render(request, 'core/index.html', {'form': "form"})


def ajax_view(request):
    api_id = "SECRET"
    api_hash = 'SECRET'
    phone = "+SECRET"

    if Channel.objects.filter(name=request.body.decode()).exists():
        return JsonResponse({'error': 'Data already collected'}, status=301)

    client = TelegramClient("first-session", api_id, api_hash)

    if request.method == 'POST':
        client.start()
        print("Client Created")
        if not client.is_user_authorized():
            client.send_code_request(phone)
            try:
                client.sign_in(phone, input('Enter the code: '))
            except SessionPasswordNeededError:
                client.sign_in(password=input('Password: '))

        me = client.get_me()

        user_input_channel = request.body.decode()

        if user_input_channel.isdigit():
            entity = client.get_input_entity(int(user_input_channel))
        else:
            entity = user_input_channel

        my_channel = client.get_entity(entity)

        offset = 0
        limit = 100
        all_participants = []
        try:
            while True:
                participants = client(functions.channels.GetParticipantsRequest(
                    my_channel, types.ChannelParticipantsSearch(''), offset, limit,
                    hash=0
                ))
                if not participants.users:
                    break
                all_participants.extend(participants.users)
                offset += len(participants.users)

            all_user_details = []
            for participant in all_participants:
                all_user_details.append(
                    {"user.id": participant.id,
                     "first_name": participant.first_name,
                     "last_name": participant.last_name,
                     "username": participant.username,
                     "phone": "a_" + str(participant.phone),
                     "user.access_hash": participant.access_hash,
                     "is_bot": participant.bot})

            collect_data.delay(all_user_details, request.body.decode())
            response_data = {'message': 'ok'}
            return JsonResponse(response_data)
        except Exception as e:
            return JsonResponse({'error': "failed"}, status=500)
    else:
        return JsonResponse({'error': 'Requête invalide'}, status=400)
